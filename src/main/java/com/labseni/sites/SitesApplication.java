package com.labseni.sites;
import com.labseni.sites.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class SitesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SitesApplication.class, args);
	}

}

