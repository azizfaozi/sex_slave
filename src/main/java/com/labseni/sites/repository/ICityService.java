package com.labseni.sites.service;

import com.labseni.sites.model.City;

import java.util.List;

public interface ICityService {

    List<City> findAll();
}
