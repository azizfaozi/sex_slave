import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import HelloComponent from './component/HelloComponent';
import StateFullComponent from './container/HelloStateFull';
import Home from './container/home/home';
ReactDOM.render(
  <App />,
  document.getElementById('root')
);

serviceWorker.unregister();
